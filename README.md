# OAuth Service
> A server-side implementation of the [OAuth 2.0](https://datatracker.ietf.org/doc/html/rfc6749#section-1.3) authorization grant flow.

## Requirements

- [Rust](https://www.rust-lang.org/)
- [Docker](https://www.docker.com/get-started)

## Usage

To install project dependencies, run the following commmand:
```bash
make install
```

To start the project, run the following command:
```bash
make start
```

To run all tests, run the following command:
```bash
make test
```

To start a local database, run the following command:
```bash
make run_local_db
```

To debug the local database, run the following command:
```bash
make debug_local_db
```

## OAuth

### [Client Registration](https://connect2id.com/products/server/docs/api/client-registration)
